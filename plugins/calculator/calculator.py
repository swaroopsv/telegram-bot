from errbot import BotPlugin, botcmd, arg_botcmd, webhook
import math
from typing import Union

class Calculator(BotPlugin):
    """
    A simple calculator
    """

    @botcmd
    def calc_add(self, message, args):
        """
        Add the numbers send as arguments and returns the value
        """
        return self.__calculate('add', args)

    @botcmd
    def calc_multiply(self, message, args):
        """
        Multiply the numbers send as arguments and returns the value
        """
        return self.__calculate('multiply', args)

    def __calculate(self, operation: str, value: str):
        """
        Extracts the numbers to be calculated and calls the respective function to do the operation

        :param operation: The calculation operation. Supports add and multiply
        :param value: The comma seperated values to be calculated
        """
        value = value.split(',')
        numbers = list()
        if len(value) > 1:
            for i in value:
                try:
                  numbers.append(int(i))
                except ValueError:
                    try:
                        numbers.append(float(i))
                    except ValueError:
                        return "Invalid input '%s' detected. Please make sure you have provided a number" % (i)
            if operation == 'add':
                return self.__add(numbers)
            elif operation == 'multiply':
                return self.__multiply(numbers)
        else:
            return "Not enough numbers to multiply"

    def __add(self, numbers: list) -> Union[float, int]:
        """
        Add all the numbers in the list

        :param numbers: list of numbers to be added
        """
        return round(sum(numbers), 2)

    def __multiply(self, numbers: list) -> Union[float, int]:
        """
        Multiply all the numbers in the list
        This method depends on prod in math which was introduced in python 3.8

        :param numbers: list of numbers to be multiplied
        """
        return round(math.prod(numbers), 2)
